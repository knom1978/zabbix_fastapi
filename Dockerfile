FROM python:3.8.2-buster

WORKDIR /code
RUN pip install --upgrade pip setuptools && \
    pip install --no-cache-dir requests fastapi pydantic uvicorn configparser
COPY ./main.py /code/main.py
COPY ./config.ini /code/config.ini
RUN mkdir /code/images
EXPOSE 5000

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "5000"]

