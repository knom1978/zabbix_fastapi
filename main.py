import requests
import os
import time
import configparser
from fastapi import FastAPI
from fastapi.responses import FileResponse

config = configparser.ConfigParser()
config.read('config.ini')

ZABBIX_HOST = config['zabbix']['host']
ZABBIX_API_URL = config['zabbix']['api_url']
ZABBIX_GRAPH_URL = config['zabbix']['graph_url']
ZABBIX_LOGIN_URL = config['zabbix']['login_url']
GRAPH_PATH = '/code/images'
UNAME = config['zabbix']['user']
PWORD = config['zabbix']['password']


description = """
## Features
- Zabbix Query API
"""

app = FastAPI(
    title="Zabbix API",
    description=description,
    version="1.0.0",
    contact={
        "name": "Cecil Liu",
        "email": "cecil_liu@umc.com",
    },
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
    )

def login_handle():
    r = requests.post(ZABBIX_API_URL,
                  json={
                      "jsonrpc": "2.0",
                      "method": "user.login",
                      "params": {
                          "user": UNAME,
                          "password": PWORD},
                      "id": 1
    })
    AUTHTOKEN = r.json()["result"]
    return AUTHTOKEN

def logout_handle(AUTHTOKEN):
    requests.post(ZABBIX_API_URL,
                  json={
                      "jsonrpc": "2.0",
                      "method": "user.logout",
                      "params": {},
                      "id": 2,
                      "auth": AUTHTOKEN
    })

@app.get("/get_host/", tags=["Zabbix"])
async def get_host():
    '''
    回傳有權限監控server的host與hostid.
    '''
    AUTHTOKEN = login_handle()
    r = requests.post(ZABBIX_API_URL,
                  json={
                      "jsonrpc": "2.0",
                      "method": "host.get",
                      "params": {
                          "output": "extend",
                       },
                      "auth": AUTHTOKEN,
                      "id": 2
        })
    host_list = []
    for i in range(len(r.json()['result'])):
        _hostid = r.json()['result'][i]['hostid']
        _host = r.json()['result'][i]['host']
        results = [{"hostid": _hostid, "host": _host}]
        host_list = host_list + results
    host_dict = dict(enumerate(host_list, start=1))
    logout_handle(AUTHTOKEN)
    return host_dict
#    return JSONResponse(jsonable_encoder(host_list))

@app.get("/get_item_by_hostid/{hostid}/{key_}", tags=["Zabbix"])
async def get_item_by_hostid(hostid:str, key_:str=None):
    '''
    輸入要查的hostid與key_, 回傳該hostid的相關的item與name等相關資訊.
    '''
    AUTHTOKEN = login_handle()
    r = requests.post(ZABBIX_API_URL,
                  json={
                       "jsonrpc": "2.0",
                       "method": "item.get",
                       "params": {
                            "output": "extend",
                            "hostids": hostid,
                            "search": {
                            "key_": key_
                       },
                       "sortfield": "name"},
                       "auth": AUTHTOKEN,
                       "id": 1
        })
    item_list = []
    for i in range(len(r.json()['result'])):
        _hostid = r.json()['result'][i]['hostid']
        _itemid = r.json()['result'][i]['itemid']
        _key_ = r.json()['result'][i]['key_']
        _name = r.json()['result'][i]['name']
        results = [{"hostid": _hostid, "itemid": _itemid, "key_": _key_, "name": _name}]
        item_list = item_list + results
    item_dict = dict(enumerate(item_list, start=1))
    logout_handle(AUTHTOKEN)
    return item_dict
#    return JSONResponse(jsonable_encoder(item_list))

@app.get("/get_graphid_by_hostid/{hostid}", tags=["Zabbix"])
async def get_graphid_by_hostid(hostid:str):
    '''
    輸入要查的hostid, 回傳該hostid的相關的graphid.
    '''
    AUTHTOKEN = login_handle()
    r = requests.post(ZABBIX_API_URL,
                  json={    
                      "jsonrpc": "2.0",
                      "method": "graph.get",
                      "params": {
                          "output": "extend",
                          "hostids": hostid,
                       "sortfield": "name"},
                       "auth": AUTHTOKEN,
                       "id": 1
                       })
    graph_list = []
    for i in range(len(r.json()['result'])):
        _graphid = r.json()['result'][i]['graphid']
        _name = r.json()['result'][i]['name']
        results = [{"hostid": hostid, "graphid": _graphid, "name": _name}]
        graph_list = graph_list + results
    graph_list = dict(enumerate(graph_list, start=1))
    logout_handle(AUTHTOKEN)
    return graph_list

@app.get("/get_image_by_graphid/{graphid}", tags=["Zabbix"])
async def get_image_by_graphid(graphid:str):
    '''
    輸入graphid, 回傳圖片.
    '''
    #Get the chart of the alarm and save it
    session=requests.Session()   #Create a session
    try:
        loginheaders={
        "Host": ZABBIX_HOST,
        "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"
        }
        #Define request headers
        payload = {
        "name": UNAME,
        "password": PWORD,
        "autologin":"1",
        "enter":"Sign in",
        }
        #Define incoming data
        session.post(url=ZABBIX_LOGIN_URL, headers=loginheaders, data=payload)
        #Login
        graph_params={
            "from" :"now-10m",
            "to" : "now",
            "graphid" : graphid,
            "width" : "400",
        }
        #Define parameters for getting pictures
        graph_req=session.get(url=ZABBIX_GRAPH_URL, params=graph_params)
        #Send get request to get picture data
        time_tag=time.strftime("%Y%m%d%H%M%S", time.localtime())
        graph_name='zabbix_'+time_tag+'.png'
        #Save with alarm time as picture name
        graph_file_path = os.path.join(GRAPH_PATH, graph_name)
        #Use absolute path to save picture
        with open(graph_file_path,'wb') as f:
            f.write(graph_req.content)
        return FileResponse(graph_file_path, media_type="image/png")
    except Exception as e:
        print(e)
        return False
