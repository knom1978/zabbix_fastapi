The idea to wrap Zabbix API by python fastapi
===
###### tags: `zabbix` `python` `fastapi`
## Introduction
參考Zabbix API 架設的 fastapi server

## Method
只需要git clone下來直接build即可.
```
docker build  --no-cache -t <<image_name>> .
```
之後接著執行.
```
docker run -p 5000:5000 <<image_name>> errbot
```

## Usage method
連線到 http://<<ip>>:5000/docs 即可使用
